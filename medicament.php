﻿<?php include("sql.php");
session_start();
if(isset($_SESSION['login']))
{
	echo "connecté en tant que: " .$_SESSION['login']."";
	echo " id_poste: " .$_SESSION['poste']."";
}
else
	{
		header('location: index.html');
	}
 ?>
<html>
	<head>
		<meta content="UTF-8">
		<title>Medicaments</title>
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body class="bodylarge">
		<table border=1>
			<tr>
				<td><img src="logo.jpg" width="100" height="60"/></td>
				<td><h1>Medicaments</h1></td>
				<!-- Bouton de déco -->
				<td><a href="deconnexion.php"><button type="button">Deconnexion</button></a></td>
			</tr>
			<tr>
				<td>
					<?php include("menu.php"); ?>
				</td>
				<td>
					<table border=1>
						<tr>
							<td><em>Nom</em></td>
							<td><em>Composition</em></td>
							<td><em>effet</em></td>
							<td><em>contrindication</em></td>
							<td><em>notice</em></td>
							<td><em>prix fab</em></td>
							<td><em>prix vente</em></td>
						</tr>
						<?php 
							// connection à la base de données
							connect("root", "", "gsb");
							// on récupère le contenu de la table medicament
							$result = requete("SELECT * FROM medicament", $bdd);
							// tant que la requete ne renvoie pas une requete vide affiche le contenu des colonnes (sauf id) dans un tableau  
							while ($row = ligne_suivante($result)) 
							{
							echo "<tr> <td>". utf8_encode($row['nom']) ."</td> <td>". utf8_encode($row['composition']) ."</td> <td>". utf8_encode($row['effet']) ."</td> <td>". utf8_encode($row['contrindication'])."</td> <td>". utf8_encode($row['notice'])."</td>  <td>". utf8_encode($row['prix_fabrication'])."</td> <td>". utf8_encode($row['prix_vente'])."</td> </tr>";
							}
						?>				
					</table>
					
				</td>
			</tr>
		</table>
	</body>
</html>