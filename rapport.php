﻿<?php include("sql.php");
session_start();
if(isset($_SESSION['login']))
{
	echo "connecté en tant que: " .$_SESSION['login']."";
}
else
	{
		header('location: index.html');
	}
 ?>
<html>
	<head>
		<meta content="UTF-8">
		<title>Rapports</title>
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body class="bodylarge">
		<table border=1>
			<tr>
				<td><h1><a href="employe.php"><img src="logo.jpg" width="100" height="60"/></h1></a></h1></td>
				<td><h1>Rapport</h1></td>
				<!-- Bouton de déco -->
				<td><a href="deconnexion.php"><button type="button">Deconnexion</button></a></td>
			</tr>
			<tr>
				<td>
					<?php include("menu.php"); ?>
				</td>
				<td>
					<table border=1>
						<tr>
							<td><em>Date</em></td>
							<td><em>Visiteur</em></td>
							<td><em>Motif de la visite</em></td>
							<td><em>Bilan</em></td>
							<td><em>Praticien</em></td>
							<td><em>Medicament</em></td>
							<td><em>quantité offerte</em></td>
						</tr>
						<?php 
							// connection à la base de données
							connect("root", "", "gsb");
							// on récupère le contenu de la table rapport_visite
							$result = requete("SELECT date, employe.nom AS employenom, employe.prenom AS employeprenom, motif, bilan, praticien.nom AS praticiennom, praticien.prenom AS praticienprenom, offrir.quantite AS offrirquantite, medicament.nom AS medicamentnom FROM `rapport_visite` INNER JOIN employe ON employe.id = rapport_visite.id_employe INNER JOIN praticien ON praticien.id = rapport_visite.id_praticien INNER JOIN offrir ON offrir.id_visite = rapport_visite.id INNER JOIN medicament ON medicament.id = offrir.id_medicament", $bdd);
							// tant que la requete ne renvoie pas une requete vide affiche le contenu des colonnes (sauf id) dans un tableau  
							while ($row = ligne_suivante($result)) 
							{
							echo "<tr> <td>". utf8_encode($row['date']) ."</td> <td>". utf8_encode($row['employenom']), utf8_encode('&nbsp;'), utf8_encode($row['employeprenom']) ."</td> <td>". utf8_encode($row['motif']) ."</td> <td>". utf8_encode($row['bilan']) ."</td> <td>". utf8_encode($row['praticiennom']), utf8_encode('&nbsp;'), utf8_encode($row['praticienprenom']) ."</td> <td>". utf8_encode($row['medicamentnom']) ."</td> <td>". utf8_encode($row['offrirquantite']) ."</td> </tr>";
							}
						?>					
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>