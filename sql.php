﻿<?php
	//fonction pour ce connecté a une base de donnée
	//prend en parametre le login, le password, et le nom de la base de donnée
	// renvoie true si on est bien connecté et false sinon
	function connect($login, $password, $bd)
	{
		global $bdd;
		$bdd = mysqli_connect("localhost",$login,$password, $bd) or die ('impossible de se connecté car : ' .mysql_error());
		//return $bdd;
	}

	//fonction pour executé une requete sql.
	//prend en parametre la fonction a éxécuté et renvoie la reponce de SQL
	function requete($requete)
	{
		global $bdd;
		$query = $requete or die("Error in the consult.." . mysqli_error($bdd));
		return $bdd->query($query);
	}

	//fonction pour recupéré les ligne qui sort de la requete 
	//prend en parametre ce que la fonction requete a renvoyé
	//a chaque fois que vous passer dans cette fonction vous passer a la ligne suivante
	function ligne_suivante($result)
	{
		if ($result)
			return mysqli_fetch_array($result);
		else 
			return NULL;
	}

	//fonction pour vous deconecté de la base de donnée
	function disconnect($login, $password, $bd)
	{
		mysql_close();//on ferme mysql 
	}
	     
?>