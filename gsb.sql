-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 24 Novembre 2014 à 06:43
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `gsb`
--

-- --------------------------------------------------------

--
-- Structure de la table `employe`    Faire une liaison avec table praticien`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `adresse` text NOT NULL,
  `code_postal` int(11) NOT NULL,
  `date_embauche` date NOT NULL,
  `id_poste` int(10) unsigned NOT NULL,
  id_praticien int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `id` (`id`),
  KEY `id_poste` (`id_poste`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `employe`
--

INSERT INTO `employe` (`id`, `login`, `password`, `nom`, `prenom`, `adresse`, `code_postal`, `date_embauche`, `id_poste`, `id_praticien`) VALUES
(1, 'DupondJ', 'test', 'Dupond', 'Jean', '109 rue de versaille', 75017, '2002-12-02', 2,2),
(2, 'SimpsonL', 'test', 'Simpson', 'Lisa', '220 rue de versaille', 13009, '2005-11-04', 2,3),
(3, 'MartinB', 'test', 'Martin', 'Bart', '51 rue de versaille', 78370, '2010-11-04', 3,1),
(4, 'MaximeL', 'test', 'Maxime', 'Lhortie', '82 rue de versaille', 91300, '2006-01-02', 3,1),
(5, 'AlexandreF', 'test', 'Alexandre', 'Felix', '225 rue de versaille', 92000, '2004-06-07', 3,2),
(6, 'KaiesC', 'test', 'Kaies', 'Chaouch', '51 rue de versaille', 78370, '2010-11-04', 2,3),
(7, 'Louis', '123', 'Louis', 'Girones', '1 rue de versaille', 75300, '2010-11-04', 2,3);

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

CREATE TABLE IF NOT EXISTS `medicament` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `composition` text,
  `effet` text,
  `contrindication` text,
  `notice` text,
  `prix_fabrication` int(10) unsigned DEFAULT NULL,
  `prix_vente` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `medicament`
--

INSERT INTO `medicament` (`id`, `nom`, `composition`, `effet`, `contrindication`, `notice`, `prix_fabrication`, `prix_vente`) VALUES
(1, 'cocaïne pharmaceutique', 'cocaïne pure', 'aléatoire', 'Ne pas donnée au personne qui on encore envie de vivre', 'Mettre la poudre dans sont nez et l''aspiré.', 10, 50);

-- --------------------------------------------------------

--
-- Structure de la table `offrir`
--

CREATE TABLE IF NOT EXISTS `offrir` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantite` int(11) NOT NULL,
  `id_visite` int(10) unsigned NOT NULL,
  `id_medicament` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_visite` (`id_visite`),
  KEY `id_medicament` (`id_medicament`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `offrir`
--

INSERT INTO `offrir` (`id`, `quantite`, `id_visite`, `id_medicament`) VALUES
(1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

CREATE TABLE IF NOT EXISTS `poste` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `poste`
--

INSERT INTO `poste` (`id`, `libelle`) VALUES
(1, 'Responsable secteur'),
(2, 'Délégué régional'),
(3, 'Visiteur');

-- --------------------------------------------------------

--
-- Structure de la table `praticien`
--

CREATE TABLE IF NOT EXISTS `praticien` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `adresse` text,
  `code_postal` int(10) unsigned DEFAULT NULL,
  `specialite` text,
  `notoriete` int(11) DEFAULT NULL COMMENT 'fixé un barème ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `praticien`
--

INSERT INTO `praticien` (`id`, `nom`, `prenom`, `adresse`, `code_postal`, `specialite`, `notoriete`) VALUES
(1, 'Leclerc', 'Paul', '2 rue du général pétin', 75000, 'Généraliste', 5),
(2, 'Wayne', 'Bruce', 'Gotham', 82210, 'urgentiste', 5),
(3, 'Kent', 'Clark', 'Metropolis', 92100, 'tout', 5);

-- --------------------------------------------------------

--
-- Structure de la table `rapport_visite`
--

CREATE TABLE IF NOT EXISTS `rapport_visite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `motif` text,
  `bilan` text,
  `impact` int(10),
  `id_praticien` int(10) unsigned NOT NULL,
  `id_employe` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_praticien` (`id_praticien`),
  KEY `id_employe` (`id_employe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `rapport_visite`
--

INSERT INTO `rapport_visite` (`id`, `date`, `motif`, `bilan`,`impact`, `id_praticien`, `id_employe`) VALUES
(1, '2011-11-03', 'présentation d''un nouveau produit', 'Après avoir testé le médecin est totalement convaincue.','4', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `rattachement`
--

CREATE TABLE IF NOT EXISTS `rattachement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_patron` int(10) unsigned NOT NULL,
  `id_employe` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_patron` (`id_patron`),
  KEY `id_employe` (`id_employe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `rattachement`
--

INSERT INTO `rattachement` (`id`, `id_patron`, `id_employe`) VALUES
(1, 1, 2),
(2, 2, 3);


--
-- Structure de la table `remplacement`
--
-- datedebut datefin, qui (nom prénom) remplace qui (idemploye) 

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
  ADD CONSTRAINT `employe_ibfk_1` FOREIGN KEY (`id_poste`) REFERENCES `poste` (`id`),
  ADD CONSTRAINT `employe_ibfk_2` FOREIGN KEY (`id_praticien`) REFERENCES `praticien` (`id`);

--
-- Contraintes pour la table `offrir`
--
ALTER TABLE `offrir`
  ADD CONSTRAINT `offrir_ibfk_1` FOREIGN KEY (`id_visite`) REFERENCES `rapport_visite` (`id`),
  ADD CONSTRAINT `offrir_ibfk_2` FOREIGN KEY (`id_medicament`) REFERENCES `medicament` (`id`);

--
-- Contraintes pour la table `rapport_visite`
--
ALTER TABLE `rapport_visite`
  ADD CONSTRAINT `rapport_visite_ibfk_1` FOREIGN KEY (`id_praticien`) REFERENCES `praticien` (`id`),
  ADD CONSTRAINT `rapport_visite_ibfk_2` FOREIGN KEY (`id_employe`) REFERENCES `employe` (`id`);

--
-- Contraintes pour la table `rattachement`
--
ALTER TABLE `rattachement`
  ADD CONSTRAINT `rattachement_ibfk_1` FOREIGN KEY (`id_patron`) REFERENCES `employe` (`id`),
  ADD CONSTRAINT `rattachement_ibfk_2` FOREIGN KEY (`id_employe`) REFERENCES `employe` (`id`);
--
-- Contraintes pour la table `praticien`
--  
  

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
