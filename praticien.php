﻿<?php include("sql.php");
session_start();
if(isset($_SESSION['login']))
{
	echo "connecté en tant que: " .$_SESSION['login']."";
}
else
	{
		header('location: index.html');
	}
 ?>
<html>
	<head>
		<meta content="UTF-8">
		<title>praticiens</title>
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body class="bodylarge">
		<table border=1>
			<tr>
				<td><img src="logo.jpg" width="100" height="60"/></td>
				<td><h1>Praticiens</h1></td>
				<!-- Bouton de déco -->
				<td><a href="deconnexion.php"><button type="button">Deconnexion</button></a></td>
			</tr>
			<tr>
				<td>
					<?php include("menu.php"); ?>
				</td>
				<td>
					<table border=1>
						<tr>
							<td><em>Nom</em></td>
							<td><em>Prenom</em></td>
							<td><em>Adresse</em></td>
							<td><em>code postal</em></td>
							<td><em>spécialité</em></td>
							<td><em>notoriété</em></td>
						</tr>
						<?php 
							// connection à la base de données
							connect("root", "", "gsb");
							// on récupère le contenu de la table praticien
							$result = requete("SELECT * FROM praticien", $bdd);
							// tant que la requete ne renvoie pas une requete vide affiche le contenu des colonnes (sauf id) dans un tableau  
							while ($row = ligne_suivante($result)) 
							{
							echo "<tr> <td>". utf8_encode($row['nom']) ."</td> <td>". utf8_encode($row['prenom']) ."</td> <td>". utf8_encode($row['adresse']) ."</td> <td>". utf8_encode($row['code_postal'])."</td> <td>". utf8_encode($row['specialite'])."</td>  <td>". utf8_encode($row['notoriete'])."</td> </tr>";
							}
						?>		
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>