﻿<?php include("sql.php");
session_start();
if(isset($_SESSION['login']))
{
	echo "connecté en tant que: " .$_SESSION['login']."";
}
else
	{
		header('location: index.html');
	}
 ?>
<html>
	<head>
		<meta content="UTF-8">
		<title>Ajout rapport</title>
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<table border=1>
			<tr>
				<td><h1><a href="employe.php"><img src="logo.jpg" width="100" height="60"/></h1></a></h1></td>
				<td><h1>Nouveau Rapport</h1></td>
				<td><a href="deconnexion.php"><button type="button">Deconnexion</button></a></td>
			</tr>
			<tr>
				<td>
					<?php include("menu.php"); ?>
				</td>
				<td>
					<form name="formVISITEUR" method="post" action="addRapp.php">
						<table>
							<tr>
								<td>Date : </td>
								<td><input type="date" name="DATE" /></td>
							</tr>
							<tr>
								<td>Motif de la visite : </td>
								
								<td>
								<select name="motifVisite">
										<option value="1">Pérodicité</option>
										<option value="2">Nouveauté</option>
										<option value="3">Actualisation</option>
								</select>
								<br/>
								<textarea rows="1" cols="40" name="MOTIF" /> </textarea></td>
							</tr>
							<tr>
								<td>Bilan : </td>
								<td><textarea rows="10" cols="40" name="BILAN" /> </textarea></td>
							</tr>
							<tr>
								<td>Praticien : </td>
								<td>
									<select name="listePraticien">
										<option value="1">Martin Bart</option>
										<option value="2">Simpson Lisa</option>
										<option value="3">Dupond Jean</option>
									</select>
								</td>
							<tr>
								<td> Impact (/10): </td>
								<td> <input type="number" size="25" maxlength="25"  name="impact" /> </td>
							</tr>
							

							<tr>
								<td></td>
								<td><input type="submit" name="envoyer" id="envoyer"/></td>
							</tr>
						</table>
					</form>
				</td>
			</tr>
		</table>
	</body>
</html>