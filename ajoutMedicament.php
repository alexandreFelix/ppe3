﻿<?php include("sql.php");
session_start();
if(isset($_SESSION['login']))
{
	echo "connecté en tant que: " .$_SESSION['login']."";
}
else
	{
		header('location: index.html');
	}
 ?>
<html>
	<head>
		<meta content="UTF-8">
		<title>Ajout Medicament</title>
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<table border=1>
			<tr>
				<td><h1><img src="logo.jpg" width="100" height="60"/></h1></td>
				<td><h1>Nouveau Medicament</h1></td>
			</tr>
			<tr>
				<td>
					<?php include("menu.php"); ?>
				</td>
				<td>
					<form name="formVISITEUR" method="post" action="addMed.php">
						<table>
							<tr>
								<td>Nom : </td>
								<td><input type="text" size="25" name="NOM" /></td>
							</tr>
							<tr>
								<td>Composition : </td>
								<td><textarea rows="6" cols="50" name="COMPOSITION" /> </textarea></td>
							</tr>
							<tr>
								<td>Effet : </td>
								<td><textarea rows="6" cols="50" name="EFFET" /> </textarea></td>
							</tr>
							<tr>
								<td>Contrindication : </td>
								<td><textarea rows="6" cols="50" name="CONTRINDICATION" /> </textarea></td>
							</tr>
							<tr>
								<td>Notice : </td>
								<td><textarea rows="6" cols="50" name="NOTICE" /> </textarea></td>
							</tr>
							<tr>
								<td>Prix de fabrication : </td>
								<td><input type="number" size="5" name="PRIXFAB" /></td>
							</tr>
							<tr>
								<td>Prix de vente : </td>
								<td><input type="number" size="5" name="PRIXVENTE" /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="envoyer" id="envoyer"/></td>
							</tr>
						</table>
					</form>
				</td>
			</tr>
		</table>
	</body>
</html>